openupalot = function(model,part)
	for i, v in pairs(model:GetChildren()) do
		if v ~= part and (v.Position-part.Position).Magnitude < 3 and v.opened.Value == false then
			v.opened.Value = true
			v.SurfaceGui.TextBox.Text = v.amount.Value
			v.CFrame = v.CFrame * CFrame.new(0,-.2,0)
			if v.amount.Value == "" then
				openupalot(model,v)
			end
		end
	end 
end

setup = function(player)
	player.Chatted:connect(function(msg)
		if msg == "/min" then
			local head = player.Character:FindFirstChild("Head")
			local size = 24
			local mines = 99
			local minesleft = mines
			if head then
				local start = CFrame.new(head.CFrame.X, 0, head.CFrame.Z)
				local tiles = {}
				start = start * CFrame.new(-size,0,-size)
				local mod = Instance.new("Model",workspace)
				mod.Name = player.Name.."'s minesweeper"
				for x = 1,size do
					for z = 1,size do
						local p = Instance.new("Part",mod)
						p.Anchored = true
						p.Locked = true
						p.Size = Vector3.new(2,1,2)
						p.CFrame = start * CFrame.new(x*2,0,z*2)
						p.Massless = true
						p.CanCollide = false
						local surfg = Instance.new("SurfaceGui",p)
						surfg.Face = "Top"
						surfg.CanvasSize = Vector2.new(150,150)
						local text = Instance.new("TextBox",surfg)
						text.Size = UDim2.new(1,0,1,0)
						text.TextScaled = true
						text.Text = ""
						text.Font = "Arcade"
						local var = Instance.new("BoolValue",p)
						var.Name = "flag"
						local var = Instance.new("BoolValue",p)
						var.Name = "opened"
						local var = Instance.new("BoolValue",p)
						var.Name = "bomb"
						local var = Instance.new("StringValue",p)
						var.Name = "amount"
						local clickd = Instance.new("ClickDetector",p)
						clickd.MouseClick:connect(function(play)
							if play == player and p.opened.Value == false and p.flag.Value == false then
								if p.bomb.Value == true then
									for i, v in pairs(mod:GetChildren()) do
										v.SurfaceGui:Destroy()
										v.BrickColor = BrickColor.Red()
										v.Material = Enum.Material.SmoothPlastic
									end
									local s = Instance.new("Sound",head)
									s.SoundId = "rbxassetid://2979857617"
									s.PlayOnRemove = true
									s.Volume = 2
									s:Destroy()
									for i, v in pairs(mod:GetChildren()) do
										v.Anchored = false
										v.Velocity = Vector3.new(math.random(-20,20),math.random(50,100),math.random(-20,20))
										v.RotVelocity = Vector3.new(math.random(-20,20),math.random(-20,20),math.random(-20,20))
										if i%size == 0 then
											wait(.2)
										end
									end
								end
								p.CFrame = p.CFrame * CFrame.new(0,-.2,0)
								p.opened.Value = true
								text.Text = p.amount.Value
								if p.amount.Value == "" then
									openupalot(mod,p)
								end
								text.TextColor3 = Color3.new(0,0,0)
							end
						end)
						clickd.RightMouseClick:connect(function(play)
							if play == player and p.opened.Value == false then
								if p.flag.Value == false then
									text.Text = "X"
									p.flag.Value = true
									text.TextColor3 = Color3.new(1,0,0)
									if p.bomb.Value == true then
										minesleft = minesleft - 1
										if minesleft == 0 then
											for i, v in pairs(mod:GetChildren()) do
												v.SurfaceGui:Destroy()
												v.BrickColor = BrickColor.Green()
												v.Material = Enum.Material.SmoothPlastic
											end
											local s = Instance.new("Sound",head)
											s.SoundId = "rbxassetid://934626738"
											s.PlayOnRemove = true
											s.Volume = 2
											s:Destroy()
											for i, v in pairs(mod:GetChildren()) do
												v.Anchored = false
												v.Velocity = Vector3.new(math.random(-20,20),math.random(50,100),math.random(-20,20))
												v.RotVelocity = Vector3.new(math.random(-20,20),math.random(-20,20),math.random(-20,20))
												if i%size == 0 then
													wait(.1)
												end
											end
										end
									end
								else
									text.Text = ""
									p.flag.Value = false
									text.TextColor3 = Color3.new(0,0,0)
									if p.bomb.Value == true then
										minesleft = minesleft + 1
									end
								end
							end
						end)
						tiles[#tiles+1] = p
					end
				end
				while mines > 0 do
					local tile = tiles[math.random(1,#tiles)]
					if tile.bomb.Value == false then
						tile.bomb.Value = true
						tile.SurfaceGui.TextBox.Text = ""
						mines = mines - 1
					end
				end
				for i, v in pairs(mod:GetChildren()) do
					if v.bomb.Value == false then
						local amount = 0
						for i, v2 in pairs(mod:GetChildren()) do
							if v ~= v2 then
								if (v.Position-v2.Position).Magnitude < 3 and v2.bomb.Value == true then
									amount = amount + 1
								end
							end
						end
						if amount > 0 then
							v.amount.Value = amount
						end
					end
				end
			end
		end
	end)
end


for i, v in pairs(game.Players:GetChildren()) do setup(v) end
game.Players.PlayerAdded:Connect(function(v) setup(v) end)